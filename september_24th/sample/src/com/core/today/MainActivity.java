/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.core.today;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.State;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.extras.SoundPullEventListener;
import com.handmark.pulltorefresh.samples.R;

public final class MainActivity extends ListActivity {

	static final int LOGIN = 0;
	static final int PROFILE = 1;
	static final int MENU_SET_MODE = 2;
	static final int MENU_DEMO = 3;


//	ArrayList<JsonNewsItem> newsItemList = new ArrayList<JsonNewsItem>();;
	private PullToRefreshListView mPullRefreshListView;
	

	
	/** Called when the activity is first created. */
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ArrayList<JsonNewsItem> m_orders = new ArrayList<JsonNewsItem>();
		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
	
		// Set a listener to be invoked when the list should be refreshed.
		mPullRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

				// Update the LastUpdatedLabel
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

				// Do work to refresh the list here.
				new GetDataTask().execute();
			}
		});

		// Add an end-of-list listener
		/*mPullRefreshListView.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

			@Override
			public void onLastItemVisible() {
				Toast.makeText(MainActivity.this, "End of List!", Toast.LENGTH_SHORT).show();
			}
		});*/
		mPullRefreshListView.setOnItemClickListener(new OnItemClickListener(){
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3){
				
				Intent intent = null;
				intent = new Intent(getBaseContext(),WholeArticle.class);
				startActivity(intent);
			}
		});
		ListView actualListView = mPullRefreshListView.getRefreshableView();
		// Need to use the Actual ListView when registering for Context Menu
		registerForContextMenu(actualListView);
		
		
		/*
		 * Json Parsing 
		*/
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
		URL url;
		HttpURLConnection urlConnection=null;
		BufferedInputStream buf=null;
		String line=null;
		String page ="";
		String a="a",b="b",c="c";
		try{
			url = new URL("http://apis.daum.net/search/knowledge?apikey=DAUM_SEARCH_DEMO_APIKEY&output=json&q=daum%20openapi");
			urlConnection = (HttpURLConnection) url.openConnection();
			buf = new BufferedInputStream(urlConnection.getInputStream());
			
			BufferedReader bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
			Log.e("sasdg","sdfsf");
			while((line=bufreader.readLine())!=null){
				page+=line;
			}
			JSONObject alljson = new JSONObject(page); 
			a=alljson.getJSONObject("channel").getString("result");
			b=alljson.getJSONObject("channel").getString("title");
			c=alljson.getJSONObject("channel").getString("totalCount");
		}
		catch(Exception e){
			
		}
		finally{
			urlConnection.disconnect();
		}



		/*String a = "abc";
		String b = "asdfas";
		String c = "asdgadsgew";
		Log.e("*********",a);*/
		m_orders.add(new JsonNewsItem(a,b,c));

		RealAdapter m_adapter = new RealAdapter(this, R.layout.listitem, m_orders); 
		m_adapter.notifyDataSetChanged();
		actualListView.setAdapter(m_adapter);
		

	
		// You can also just use setListAdapter(mAdapter) or
		// mPullRefreshListView.setAdapter(mAdapter)try{
		//try{
			Toast tost = Toast.makeText(getApplicationContext(),"adsfadsfadsfsfa",1000);
			tost.show();
		//JSONObject json = new JSONObject(strJson);
	//	JSONArray jArr = json.getJSONArray("address");
		//Toast tost = Toast.makeText(getApplicationContext(),"adsfadsfadsfsfa",1000);
	//	tost.show();
	//	for(int i =0; i<jArr.length(); i++){
			
		//	json=jArr.getJSONObject(i);
			//json.getString("state");

		
	
		//	newsItemList.add(new JsonNewsItem(a,b,c));
//		}		
		
	//	} catch(JSONException e){
	//		Log.d("Error",e.getMessage());
	//	}
			
//	}
	}
	private class GetDataTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			// Simulates a background job.
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
			}
			return strJson;
		}

		@Override

		protected void onPostExecute(String result) {
			//newsItemList.addFirst("Added after refresh...");
		//	m_adapter.notifyDataSetChanged();

			// Call onRefreshComplete when the list has been refreshed.
			mPullRefreshListView.onRefreshComplete();

			super.onPostExecute(result);
		}
	}


	private class RealAdapter extends ArrayAdapter<JsonNewsItem> {
		private ArrayList<JsonNewsItem> items;
		
		public RealAdapter(Context context, int textViewResourceId, ArrayList<JsonNewsItem> items) {
			super(context, textViewResourceId, items);
			this.items = items;
		}
	    
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {    
			View v = convertView;     
			//if (v == null) {      
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);     
	                	v = vi.inflate(R.layout.listitem, null);          
			//} 
			JsonNewsItem p = items.get(position);     
	        	if (p != null) {                 
			TextView tt = (TextView) v.findViewById(R.id.dataItem01);        
			TextView bt = (TextView) v.findViewById(R.id.dataItem02);
			TextView ct = (TextView) v.findViewById(R.id.dataItem03);
			
			//if (tt != null){                 
				tt.setText(p.getTitle());                                   
			//}
			//if(bt != null){                  
				bt.setText(p.getLink());             
			//}
			//if(ct != null){                  
				ct.setText(p.getDescription());      
			//} 
		}            
		return v;   
	} 
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, LOGIN, 0, "Login to Core.2day");
		menu.add(0, PROFILE, 0, "My Profile");
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;

		menu.setHeaderTitle("Item: " + getListView().getItemAtPosition(info.position));
		menu.add("Item 1");
		menu.add("Item 2");
		menu.add("Item 3");
		menu.add("Item 4");

		super.onCreateContextMenu(menu, v, menuInfo);
	}

	//@Override
	//public boolean onPrepareOptionsMenu(Menu menu) {
		/*MenuItem disableItem = menu.findItem(MENU_DISABLE_SCROLL);
		disableItem
				.setTitle(mPullRefreshListView.isScrollingWhileRefreshingEnabled() ? "Disable Scrolling while Refreshing"
						: "Enable Scrolling while Refreshing");

		MenuItem setModeItem = menu.findItem(MENU_SET_MODE);
		setModeItem.setTitle(mPullRefreshListView.getMode() == Mode.BOTH ? "Change to MODE_FROM_START"
				: "Change to MODE_PULL_BOTH");

		return super.onPrepareOptionsMenu(menu);*/
	//}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case LOGIN:
				Intent login_intent = null;
				login_intent = new Intent(MainActivity.this, Login.class);
				startActivity(login_intent);
			    break;
			case PROFILE:
				
				break;
			case MENU_SET_MODE:
				mPullRefreshListView.setMode(mPullRefreshListView.getMode() == Mode.BOTH ? Mode.PULL_FROM_START
						: Mode.BOTH);
				break;
			case MENU_DEMO:
				mPullRefreshListView.demo();
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	/*private String[] mStrings = { "Abbaye de Belloc", "Abbaye du Mont des Cats", "Abertam", "Abondance", "Ackawi",
			"Acorn", "Adelost", "Affidelice au Chablis", "Afuega'l Pitu", "Airag", "Airedale", "Aisy Cendre",
			"Allgauer Emmentaler", "Abbaye de Belloc", "Abbaye du Mont des Cats", "Abertam", "Abondance", "Ackawi",
			"Acorn", "Adelost", "Affidelice au Chablis", "Afuega'l Pitu", "Airag", "Airedale", "Aisy Cendre",
			"Allgauer Emmentaler" };*/
	
	String strJson = "{\"address\":\"[{\"state\":\"Alabama\",\"capital\":\"Montgomery\",\"latitude\":\"32.361538\",\"longitude\":\"-86.279118\"}," +
			"{\"state\":\"Alaska\",\"capital\":\"Juneau\",\"latitude\":\"58.301935\",\"longitude\":\"-134.419740\"},"+
            "{\"state\":\"Arizona\",\"capital\":\"Phoenix\",\"latitude\":\"33.448457\",\"longitude\":\"112.073844\"},"+
            "{\"state\":\"California\",\"capital\":\"Secramento\",\"latitude\":\"36.448457\",\"longitude\":\"112.073844\"},"+
            "{\"state\":\"Gangwon\",\"capital\":\"Chunchon\",\"latitude\":\"33.448457\",\"longitude\":\"112.073844\"},"+
            "{\"state\":\"Rhode Island\",\"capital\":\"Providence\",\"latitude\":\"33.448457\",\"longitude\":\"112.073844\"},"+
            "{\"state\":\"New Jersey\",\"capital\":\"Trenton\",\"latitude\":\"33.448457\",\"longitude\":\"112.073844\"}]}";
}
